package com.cft.pojo;

import java.util.List;

/**
 * POJO for Shipper
 * 
 * @author Mirza Rahaman
 *
 */
public class Shipper {

	private String id;
	private List<String> shipments;
	private String last_checked_time;
	private String email;

	/**
	 * Constructor
	 * 
	 * @param id
	 * @param shipments
	 * @param last_checked_time
	 * @param email
	 */
	public Shipper(String id, List<String> shipments, String last_checked_time, String email) {

		this.setId(id);
		this.setShipments(shipments);
		this.setLast_checked_time(last_checked_time);
		this.setEmail(email);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the shipments
	 */
	public List<String> getShipments() {
		return shipments;
	}

	/**
	 * @param shipments
	 *            the shipments to set
	 */
	public void setShipments(List<String> shipments) {
		this.shipments = shipments;
	}

	/**
	 * @return the last_checked_time
	 */
	public String getLast_checked_time() {
		return last_checked_time;
	}

	/**
	 * @param last_checked_time
	 *            the last_checked_time to set
	 */
	public void setLast_checked_time(String last_checked_time) {
		this.last_checked_time = last_checked_time;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
}
