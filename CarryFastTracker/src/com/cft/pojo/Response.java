package com.cft.pojo;

/**
 * POJO for API response
 * 
 * @author Mirza Rahaman
 *
 */
public class Response {

	private String Status;
	private String Time;

	/**
	 * Constructor
	 * 
	 * @param Status
	 * @param Time
	 */
	public Response(String Status, String Time) {

		this.setStatus(Status);
		this.setTime(Time);
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return Time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time) {
		Time = time;
	}
}
