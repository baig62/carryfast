package com.cft.pojo;

import java.util.List;

/**
 * POJO for Destination
 * 
 * @author Mirza Rahaman
 *
 */
public class Destination {

	/**
	 * POJO for Arrival
	 * 
	 * @author Mirza Rahaman
	 *
	 */
	public class Arrival {

		private String TimeStamp;
		private List<String> expectedShipments;

		/**
		 * Constructor
		 * 
		 * @param TimeStamp
		 * @param expectedShipments
		 */
		public Arrival(String TimeStamp, List<String> expectedShipments) {

			this.setTimeStamp(TimeStamp);
			this.setExpectedShipments(expectedShipments);
		}

		/**
		 * @return the timeStamp
		 */
		public String getTimeStamp() {
			return TimeStamp;
		}

		/**
		 * @param timeStamp
		 *            the timeStamp to set
		 */
		public void setTimeStamp(String timeStamp) {
			TimeStamp = timeStamp;
		}

		/**
		 * @return the expectedShipments
		 */
		public List<String> getExpectedShipments() {
			return expectedShipments;
		}

		/**
		 * @param expectedShipments
		 *            the expectedShipments to set
		 */
		public void setExpectedShipments(List<String> expectedShipments) {
			this.expectedShipments = expectedShipments;
		}
	}

	private String id;
	private List<Arrival> arrivals;

	/**
	 * 
	 * @param id
	 * @param arrivals
	 */
	public Destination(String id, List<Arrival> arrivals) {

		this.setId(id);
		this.setArrivals(arrivals);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the arrivals
	 */
	public List<Arrival> getArrivals() {
		return arrivals;
	}

	/**
	 * @param arrivals
	 *            the arrivals to set
	 */
	public void setArrivals(List<Arrival> arrivals) {
		this.arrivals = arrivals;
	}
}
