package com.cft.pojo;

import java.util.List;

/**
 * POJO for shipment
 * 
 * @author Mirza Rahaman
 *
 */
public class Shipment {

	private String id;
	private String last_seen_location;
	private List<String> next_destinations;
	private String last_seen_time;

	/**
	 * Constructor
	 * 
	 * @param id
	 * @param last_seen_location
	 * @param next_destinations
	 * @param last_seen_time
	 */
	public Shipment(String id, String last_seen_location, List<String> next_destinations, String last_seen_time) {

		this.setId(id);
		this.setLast_seen_location(last_seen_location);
		this.setNext_destinations(next_destinations);
		this.setLast_seen_time(last_seen_time);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the last_seen_location
	 */
	public String getLast_seen_location() {
		return last_seen_location;
	}

	/**
	 * @param last_seen_location
	 *            the last_seen_location to set
	 */
	public void setLast_seen_location(String last_seen_location) {
		this.last_seen_location = last_seen_location;
	}

	/**
	 * @return the next_destinations
	 */
	public List<String> getNext_destinations() {
		return next_destinations;
	}

	/**
	 * @param next_destinations
	 *            the next_destinations to set
	 */
	public void setNext_destinations(List<String> next_destinations) {
		this.next_destinations = next_destinations;
	}

	/**
	 * @return the last_seen_time
	 */
	public String getLast_seen_time() {
		return last_seen_time;
	}

	/**
	 * @param last_seen_time
	 *            the last_seen_time to set
	 */
	public void setLast_seen_time(String last_seen_time) {
		this.last_seen_time = last_seen_time;
	}
}
