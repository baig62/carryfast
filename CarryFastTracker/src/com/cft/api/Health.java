package com.cft.api;

import java.text.ParseException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.cft.util.DateUtil;
import com.cft.util.Gson;

/**
 * API for health check
 * 
 * @author Mirza Rahaman
 *
 */
@Path("/health")
public class Health {

	/**
	 * API to check if the app is up and running
	 * 
	 * @return
	 * @throws ParseException
	 */
	@GET
	@Produces("application/json")
	public Response getHealth() throws ParseException {

		com.cft.pojo.Response response = new com.cft.pojo.Response("OK", DateUtil.getCurrentDate());
		return Response.status(200).entity(Gson.toJson(response)).build();
	}
}
