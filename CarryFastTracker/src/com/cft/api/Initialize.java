package com.cft.api;

import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cft.pojo.Destination;
import com.cft.pojo.Shipment;
import com.cft.pojo.Shipper;
import com.cft.util.DateUtil;
import com.cft.util.Gson;
import com.cft.util.Scheduler;

/**
 * Initialize destination, shipper and shipment maps mimicking db
 * 
 * @author Mirza Rahaman
 *
 */
@Path("/init")
public class Initialize {

	/**
	 * Initialize destinations
	 * 
	 * @param json
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("destinations")
	public Response initDestination(String json) {

		Destination[] destinations = Gson.fromJson(json, Destination[].class);

		if (com.cft.dao.Destination.destinations == null) {

			com.cft.dao.Destination.destinations = new ArrayList<>(Arrays.asList(destinations));
		}

		com.cft.pojo.Response response = new com.cft.pojo.Response("OK", DateUtil.getCurrentDate());
		return Response.status(201).entity(Gson.toJson(response)).build();
	}

	/**
	 * Initialize shippers
	 * 
	 * @param json
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("shippers")
	public Response initShipper(String json) {

		Shipper[] shippers = Gson.fromJson(json, Shipper[].class);

		if (com.cft.dao.Shipper.shippers == null) {

			com.cft.dao.Shipper.shippers = new ArrayList<>(Arrays.asList(shippers));
		}

		com.cft.pojo.Response response = new com.cft.pojo.Response("OK", DateUtil.getCurrentDate());
		return Response.status(201).entity(Gson.toJson(response)).build();
	}

	/**
	 * Initialize shipments
	 * 
	 * @param json
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("shipments")
	public Response initShipment(String json) {

		Shipment[] shipments = Gson.fromJson(json, Shipment[].class);

		if (com.cft.dao.Shipment.shipments == null) {

			com.cft.dao.Shipment.shipments = new ArrayList<>(Arrays.asList(shipments));
		}

		// This API is trigger to start agents
		Scheduler.getInstance().emailAgent();
		System.out.println("Email agent started");

		Scheduler.getInstance().shipmentAgent();
		System.out.println("Shipment agent started");

		Scheduler.getInstance().emitterAgent();
		System.out.println("Emitter agent started");

		com.cft.pojo.Response response = new com.cft.pojo.Response("OK", DateUtil.getCurrentDate());
		return Response.status(201).entity(Gson.toJson(response)).build();
	}
}
