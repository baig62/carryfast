package com.cft.util;

import java.lang.reflect.Type;

/**
 * Utility to convert JSON to POJO and vice versa
 * 
 * @author Mirza Rahaman
 *
 */
public class Gson {

	/**
	 * Convert JSON to POJO
	 * 
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> T fromJson(String json, Type clazz) {

		com.google.gson.Gson gson = new com.google.gson.Gson();
		return gson.fromJson(json, clazz);
	}

	/**
	 * Convert POJO to JSON
	 * 
	 * @param object
	 * @return
	 */
	public static String toJson(Object object) {

		com.google.gson.Gson gson = new com.google.gson.Gson();
		return gson.toJson(object);
	}
}
