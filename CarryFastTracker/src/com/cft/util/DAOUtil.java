package com.cft.util;

import java.util.List;

import com.cft.dao.Destination;
import com.cft.pojo.Destination.Arrival;
import com.cft.pojo.Shipper;

/**
 * Mimicking dao where the datasources is maps
 * 
 * @author Mirza Rahaman
 *
 */
public class DAOUtil {

	/**
	 * Get destination for shipmentId and destinationId
	 * 
	 * @param shipmentId
	 * @param destinationId
	 * @return
	 */
	public static com.cft.pojo.Destination getDestination(String shipmentId, String destinationId) {

		for (com.cft.pojo.Destination destination : Destination.destinations) {

			if (destinationId.equals(destination.getId())) {

				return destination;
			}
		}

		return null;
	}

	/**
	 * Get arrival timestamp for shipmentId and destination
	 * 
	 * @param shipmentId
	 * @param destination
	 * @return
	 */
	public static String getArrivalTimestamp(String shipmentId, com.cft.pojo.Destination destination) {

		List<Arrival> arrivals = destination.getArrivals();
		for (Arrival arrival : arrivals) {

			if (arrival.getExpectedShipments().contains(shipmentId)) {

				return arrival.getTimeStamp();
			}
		}

		return null;
	}

	/**
	 * Get shipper email for shipmentId
	 * 
	 * @param shipmentId
	 * @return
	 */
	public static String getEmail(String shipmentId) {

		for (Shipper shipper : com.cft.dao.Shipper.shippers) {

			if (shipper.getShipments().contains(shipmentId)) {

				return shipper.getEmail();
			}
		}

		return null;
	}
}
