package com.cft.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date utility
 * 
 * @author Mirza Rahaman
 *
 */
public class DateUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss");

	/**
	 * Get current date
	 * 
	 * @return
	 */
	public static String getCurrentDate() {

		return sdf.format(new Date());
	}

	/**
	 * Compare two date strings
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * @throws ParseException
	 */
	public static int compareTo(String date1, String date2) throws ParseException {

		Date d1 = parseDate(date1);
		Date d2 = parseDate(date2);

		return d1.compareTo(d2);
	}

	/**
	 * Get difference in seconds among two date strings
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 * @throws ParseException
	 */
	public static long getDiffSecs(String date1, String date2) throws ParseException {

		Date d1 = parseDate(date1);
		Date d2 = parseDate(date2);

		return (d1.getTime() - d2.getTime()) / 1000;
	}

	/**
	 * Parse date string to date
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static Date parseDate(String date) throws ParseException {

		return sdf.parse(date);
	}
}
