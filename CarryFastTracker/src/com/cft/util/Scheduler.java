package com.cft.util;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.cft.dao.Shipment;
import com.cft.pojo.Destination;
import com.cft.queue.Queue;

/**
 * Scheduler which schedules tasks using ScheduledExecutorService
 * 
 * @author Mirza Rahaman
 *
 */
public class Scheduler {

	private static Scheduler scheduler;
	private boolean emitterAgentRunning;
	private boolean shipmentAgentRunning;
	private boolean emailAgentRunning;

	/**
	 * Constructor
	 */
	private Scheduler() {

	}

	/**
	 * Singleton object
	 * 
	 * @return
	 */
	public static Scheduler getInstance() {

		if (Queue.emailQueue == null) {

			Queue.emailQueue = new HashMap<>();
		}

		if (Queue.shipmentQueue == null) {

			Queue.shipmentQueue = new LinkedHashMap<>();
		}

		if (scheduler == null) {

			scheduler = new Scheduler();
		}

		return scheduler;
	}

	/**
	 * Emitter agent which will keep on dropping message to shipments queue
	 */
	public void emitterAgent() {

		if (!emitterAgentRunning) {

			ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

			Runnable task = () -> {

				System.out.println("Emitter agent, shipments: " + Gson.toJson(Shipment.shipments));

				if (Shipment.shipments.size() > 0) {

					com.cft.pojo.Shipment shipment = Shipment.shipments.get(0);

					Queue.shipmentQueue.put(UUID.randomUUID().toString(), shipment);

					Shipment.shipments.remove(0);
				}
			};

			executor.scheduleWithFixedDelay(task, 5, 5, TimeUnit.SECONDS);

			emitterAgentRunning = true;
		}
	}

	/**
	 * Shipment agent which processes the entries in shipments queue
	 */
	public void shipmentAgent() {

		if (!shipmentAgentRunning) {

			ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

			Runnable task = () -> {

				System.out.println("Shipment agent, current queue: " + Gson.toJson(Queue.shipmentQueue));

				Iterator<Entry<String, Object>> iter = Queue.shipmentQueue.entrySet().iterator();
				while (iter.hasNext()) {

					Entry<String, Object> entry = iter.next();

					com.cft.pojo.Shipment shipment = (com.cft.pojo.Shipment) Queue.shipmentQueue.get(entry.getKey());

					List<String> next_destinations = shipment.getNext_destinations();
					if (next_destinations.size() > 0) {

						String destinationId = next_destinations.get(0);
						Destination destination = DAOUtil.getDestination(shipment.getId(), destinationId);
						String timestamp = DAOUtil.getArrivalTimestamp(shipment.getId(), destination);

						try {
							long diffSecs = DateUtil.getDiffSecs(shipment.getLast_seen_time(), timestamp);
							if (Math.abs(diffSecs) == 45) {

								Queue.emailQueue.put("45" + UUID.randomUUID(), shipment.getId() + "-" + destinationId
										+ "-" + DAOUtil.getEmail(shipment.getId()));
							} else if (Math.abs(diffSecs) == 30) {

								Queue.emailQueue.put("30" + UUID.randomUUID(), shipment.getId() + "-" + destinationId
										+ "-" + DAOUtil.getEmail(shipment.getId()));
							} else if (Math.abs(diffSecs) == 0) {

								Queue.emailQueue.put("00" + UUID.randomUUID(), shipment.getId() + "-" + destinationId
										+ "-" + DAOUtil.getEmail(shipment.getId()));
							}
						} catch (ParseException e) {

						}
					}

					iter.remove();
				}
			};

			executor.scheduleWithFixedDelay(task, 1, 1, TimeUnit.SECONDS);

			shipmentAgentRunning = true;
		}
	}

	/**
	 * Email agent which send email on designated time slots
	 */
	public void emailAgent() {

		if (!emailAgentRunning) {

			ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

			Runnable task = () -> {

				System.out.println("Email agent, current email queue: " + Gson.toJson(Queue.emailQueue));

				Iterator<Entry<String, String>> iter = Queue.emailQueue.entrySet().iterator();
				while (iter.hasNext()) {

					Entry<String, String> entry = iter.next();

					String[] content = entry.getValue().split("-");
					System.out.println(
							"Email sent for shipment " + content[0] + " before reaching destination " + content[1]
									+ " to " + content[2] + " before " + entry.getKey().substring(0, 2) + " seconds");

					iter.remove();
				}
			};

			executor.scheduleWithFixedDelay(task, 1, 1, TimeUnit.SECONDS);

			emailAgentRunning = true;
		}
	}
}
