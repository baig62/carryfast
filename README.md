# CarryFast

# maven package
# deploy on Tomcat
# logging via sysout

Follow the below steps for dry run, after starting server, hit the following APIs in order,

POST http://127.0.0.1:8080/CarryFastTracker/cf/init/destinations
[
	{
		"id": "d0",
		"arrivals":[
			{
				"TimeStamp" : "08/02/2018, 12:00:00",
				"expectedShipments" : ["s1234"]
			}
		]
	},
	{
		"id": "d10",
		"arrivals":[
			{
				"TimeStamp" : "08/02/2018, 12:01:00",
				"expectedShipments" : ["s1234"]
			}
		]
	}
]

POST http://127.0.0.1:8080/CarryFastTracker/cf/init/shippers
[
	{
		"id" : "Su3456",
		"shipments" : ["s1234"],
		"last_checked_time" : "05/01/2018, 12:27:27",
		"email": "xyz@abc.com"
	}
]

POST http://127.0.0.1:8080/CarryFastTracker/cf/init/shipments
[
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:00"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:05"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:10"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:15"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:20"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:25"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:30"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:35"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:40"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:45"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:50"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:00:55"
	},
	{
		"id" : "s1234",
		"last_seen_location": "d0",
		"next_destinations" : ["d10"],
		"last_seen_time" : "08/02/2018, 12:01:00"
	}
]

Monitor the console for email log